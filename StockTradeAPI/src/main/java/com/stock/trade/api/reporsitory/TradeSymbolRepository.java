package com.stock.trade.api.reporsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stock.trade.api.domain.TradeSymbol;

public interface TradeSymbolRepository extends JpaRepository<TradeSymbol, Long> {

	public boolean existsBySymbol(String symbol);
}
