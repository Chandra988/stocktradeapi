package com.stock.trade.api.reporsitory;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stock.trade.api.domain.Stock;
import com.stock.trade.api.domain.User;

public interface StockRepository extends JpaRepository<Stock, Long> {

	public List<Stock> findByUser(User user);

	public List<Stock> findBySymbolAndTimestampBetween(String symbol, LocalDateTime from, LocalDateTime to);

	public List<Stock> findByTimestampBetween(LocalDateTime from, LocalDateTime to);
	
}
