package com.stock.trade.api.constants;

public class URLConstants {

	private URLConstants() {

	}
	
	public static final String USER_IP_ADDRESS = "http://localhost:";
	public static final String USER_PORT_NUMBER = "10231";
	
	public static final String URL = USER_IP_ADDRESS + USER_PORT_NUMBER;
	
	//Find all stocks
	public static final String ALL_TRADES = "/trades";
	public static final String FIND_TRADES_WITH_USER_ID = "/trades/users/{0}";
}