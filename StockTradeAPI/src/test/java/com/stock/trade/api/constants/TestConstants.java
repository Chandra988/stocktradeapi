package com.stock.trade.api.constants;
public class TestConstants {

	private TestConstants() {
		
	}
	
	public static final String DEFAULT_CREATED_BY = "A";
	public static final String DEFAULT_UPDATED_BY = "AB";
	public static final String TEST_PROFILE = "test";
}
