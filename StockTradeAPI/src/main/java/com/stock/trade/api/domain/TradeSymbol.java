package com.stock.trade.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SYMBOL")
@SequenceGenerator(name = "dbSequence", sequenceName = "SYMBOL_SEQ", allocationSize = 1, initialValue = 1)
public class TradeSymbol implements Serializable {

	private static final long serialVersionUID = 690411376796191283L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dbSequence")
	@Column(name = "SYMBOL_ID")
	private Long id;
	@Column(name = "STOCK_SYMBOL")
	private String symbol;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
