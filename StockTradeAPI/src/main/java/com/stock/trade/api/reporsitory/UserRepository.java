package com.stock.trade.api.reporsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stock.trade.api.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {

	public User findByNameAndId(String userName, Long id);

	public User findByName(String userName);
}
