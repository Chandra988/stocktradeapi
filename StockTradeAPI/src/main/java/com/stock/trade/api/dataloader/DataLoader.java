package com.stock.trade.api.dataloader;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.stock.trade.api.domain.Stock;
import com.stock.trade.api.domain.TradeSymbol;
import com.stock.trade.api.domain.User;
import com.stock.trade.api.reporsitory.TradeSymbolRepository;
import com.stock.trade.api.reporsitory.UserRepository;
import com.stock.trade.api.rest.controller.StockController;

@Component
public class DataLoader implements CommandLineRunner {
	@Autowired
	private StockController stockRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TradeSymbolRepository tradeSymbolRepository;

	@Override
	public void run(String... strings) throws Exception {

		// Initializing trade symbols
		TradeSymbol symbol = new TradeSymbol();
		symbol.setSymbol("AC");
		tradeSymbolRepository.save(symbol);
		symbol = new TradeSymbol();
		symbol.setSymbol("ACC");
		tradeSymbolRepository.save(symbol);
		symbol = new TradeSymbol();
		symbol.setSymbol("ABR");
		tradeSymbolRepository.save(symbol);

		// Initialize user
		User user1 = new User();
		user1.setName("David");
		userRepository.save(user1);
		User user2 = new User();
		user2.setName("Omar");
		userRepository.save(user2);
		User user3 = new User();
		user3.setName("Brandon");
		userRepository.save(user3);

		// Initialize stock
		Stock stock1 = new Stock();
		stock1.setType("Buy");
		stock1.setPrice(BigDecimal.valueOf(162.17));
		stock1.setSymbol("AC");
		stock1.setShares(28);
		stock1.setTimestamp(LocalDateTime.of(2014, 06, 14, 13, 13, 13));
		User david = userRepository.findByName("David");
		stock1.setUser(david);
		stockRepository.addStock(stock1);

		Stock stock2 = new Stock();
		stock2.setType("Buy");
		stock2.setPrice(BigDecimal.valueOf(146.09));
		stock2.setSymbol("ACC");
		stock2.setShares(25);
		stock2.setTimestamp(LocalDateTime.of(2014, 06, 25, 13, 40, 13));
		User user = userRepository.findByName("Brandon");
		stock2.setUser(user);
		stockRepository.addStock(stock2);

		Stock stock3 = new Stock();
		stock3.setType("Buy");
		stock3.setPrice(BigDecimal.valueOf(146.09));
		stock3.setSymbol("AC");
		stock3.setShares(13);
		stock3.setTimestamp(LocalDateTime.of(2014, 06, 25, 13, 40, 13));
		user = userRepository.findByName("Omar");
		stock3.setUser(user);
		stockRepository.addStock(stock3);

		Stock stock4 = new Stock();
		stock4.setType("Buy");
		stock4.setPrice(BigDecimal.valueOf(137.39));
		stock4.setSymbol("AC");
		stock4.setShares(12);
		stock4.setTimestamp(LocalDateTime.of(2014, 06, 25, 13, 44, 13));
		stock4.setUser(david);
		stockRepository.addStock(stock4);

		Stock stock5 = new Stock();
		stock5.setType("Buy");
		stock5.setPrice(BigDecimal.valueOf(161.35));
		stock5.setSymbol("AC");
		stock5.setShares(15);
		stock5.setTimestamp(LocalDateTime.of(2014, 06, 26, 13, 15, 18));
		user = userRepository.findByName("Brandon");
		stock5.setUser(user);
		stockRepository.addStock(stock5);

		Stock stock6 = new Stock();
		stock6.setType("Sell");
		stock6.setPrice(BigDecimal.valueOf(162.37));
		stock6.setSymbol("AC");
		stock6.setShares(10);
		stock6.setTimestamp(LocalDateTime.of(2014, 06, 26, 15, 15, 18));
		user = userRepository.findByName("Brandon");
		stock6.setUser(user);
		stockRepository.addStock(stock6);

		Stock stock7 = new Stock();
		stock7.setType("Buy");
		stock7.setPrice(BigDecimal.valueOf(146.08));
		stock7.setSymbol("ACC");
		stock7.setShares(17);
		stock7.setTimestamp(LocalDateTime.of(2014, 06, 27, 10, 10, 31));
		user = userRepository.findByName("Brandon");
		stock7.setUser(user);
		stockRepository.addStock(stock7);

		Stock stock8 = new Stock();
		stock8.setType("Buy");
		stock8.setPrice(BigDecimal.valueOf(146.11));
		stock8.setSymbol("ACC");
		stock8.setShares(15);
		stock8.setTimestamp(LocalDateTime.of(2014, 06, 27, 11, 8, 23));
		user = userRepository.findByName("Brandon");
		stock8.setUser(user);
		stockRepository.addStock(stock8);

		Stock stock9 = new Stock();
		stock9.setType("Buy");
		stock9.setPrice(BigDecimal.valueOf(146.09));
		stock9.setSymbol("ACC");
		stock9.setShares(25);
		stock9.setTimestamp(LocalDateTime.of(2014, 06, 27, 12, 17, 17));
		user = userRepository.findByName("Brandon");
		stock9.setUser(user);
		stockRepository.addStock(stock9);

		Stock stock10 = new Stock();
		stock10.setType("Buy");
		stock10.setPrice(BigDecimal.valueOf(136.27));
		stock10.setSymbol("ABR");
		stock10.setShares(10);
		stock10.setTimestamp(LocalDateTime.of(2014, 06, 28, 13, 11, 13));
		user = userRepository.findByName("David");
		stock10.setUser(user);
		stockRepository.addStock(stock10);

	}
}