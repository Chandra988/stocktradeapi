package com.stock.trade.api.utils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public class RestTemplateUtil {
	
	private RestTemplateUtil() { }

	public static RestTemplate getRestTemplate() {
		
		RestTemplate restTemplate = new RestTemplate();
		
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		List<MediaType> supportedMediaTypes = new ArrayList<>();
		MediaType mediaType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName(StandardCharsets.UTF_8.name()));
		supportedMediaTypes.add(mediaType);
		MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
		jacksonConverter.setSupportedMediaTypes(supportedMediaTypes);
		messageConverters.add(jacksonConverter);
		restTemplate.setMessageConverters(messageConverters);
		
		return restTemplate;
	}
	
	public static HttpEntity<Object> getCreateHeaders(Object params) {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<Object> entity = null;
		if (params == null) {
			entity = new HttpEntity<>(headers);
		} else {
			entity = new HttpEntity<>(params, headers);
		}
		return entity;
	}
	
}