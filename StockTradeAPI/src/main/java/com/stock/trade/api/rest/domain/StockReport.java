package com.stock.trade.api.rest.domain;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class StockReport {

	@JsonInclude(Include.NON_NULL)
	private String stock;
	@JsonInclude(Include.NON_NULL)
	private Integer fluctuations;
	@JsonInclude(Include.NON_NULL)
	private BigDecimal max_rise;
	@JsonInclude(Include.NON_NULL)
	private BigDecimal max_fall;
	@JsonInclude(Include.NON_NULL)
	private String message;

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public Integer getFluctuations() {
		return fluctuations;
	}

	public void setFluctuations(Integer fluctuations) {
		this.fluctuations = fluctuations;
	}

	public BigDecimal getMax_rise() {
		return max_rise;
	}

	public void setMax_rise(BigDecimal max_rise) {
		this.max_rise = max_rise;
	}

	public BigDecimal getMax_fall() {
		return max_fall;
	}

	public void setMax_fall(BigDecimal max_fall) {
		this.max_fall = max_fall;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
