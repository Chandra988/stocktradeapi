package com.stock.trade.api.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "USER")
@SequenceGenerator(name = "dbSequence", sequenceName = "USER_SEQ", allocationSize = 1, initialValue = 1)
public class User implements Serializable {

	private static final long serialVersionUID = -6961874969634836992L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dbSequence")
	@Column(name = "USER_ID")
	private Long id;
	@Column(name = "USER_NAME")
	private String name;
	@JoinColumn(name = "USER_ID")
	@OneToMany(fetch = FetchType.LAZY)
	@JsonBackReference
	private Set<Stock> stock;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Stock> getStock() {
		return stock;
	}

	public void setStock(Set<Stock> stock) {
		this.stock = stock;
	}

}
