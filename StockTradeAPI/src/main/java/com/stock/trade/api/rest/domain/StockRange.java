package com.stock.trade.api.rest.domain;

import java.math.BigDecimal;

public class StockRange {

	private String symbol;
	private BigDecimal highest;
	private BigDecimal lowest;

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public BigDecimal getHighest() {
		return highest;
	}

	public void setHighest(BigDecimal highest) {
		this.highest = highest;
	}

	public BigDecimal getLowest() {
		return lowest;
	}

	public void setLowest(BigDecimal lowest) {
		this.lowest = lowest;
	}
}
