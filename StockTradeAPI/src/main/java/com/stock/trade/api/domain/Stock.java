package com.stock.trade.api.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "STOCK")
@SequenceGenerator(name = "dbSequence", sequenceName = "STOCK_SEQ", allocationSize = 1, initialValue = 1)
@FilterDef(name = "deletedFilter")
@Filters({ @Filter(name = "deletedFilter", condition = "IS_DELETED_IND = 'N'") })
@Where(clause = "IS_DELETED_IND <> 'Y'")
public class Stock implements Serializable {

	private static final long serialVersionUID = 8848331840850266684L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dbSequence")
	@Column(name = "STOCK_ID")
	private Long id;
	@Column(name = "STOCK_TYPE")
	private String type;
	@Column(name = "STOCK_SYMBOL")
	private String symbol;
	@Column(name = "STOCK_SHARES")
	private Integer shares;
	@Column(name = "STOCK_PRICE")
	private BigDecimal price;
	@Column(name = "STOCK_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime timestamp;
	@Column(name = "IS_DELETED_IND")
	@Type(type = "yes_no")
	private Boolean isDeleted;
	@Column(name = "DELETED_ON")
	private LocalDate deletedOn;
	@JoinColumn(name = "USER_ID")
	@ManyToOne(fetch = FetchType.EAGER)
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Integer getShares() {
		return shares;
	}

	public void setShares(Integer shares) {
		this.shares = shares;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public LocalDate getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(LocalDate deletedOn) {
		this.deletedOn = deletedOn;
	}

}
