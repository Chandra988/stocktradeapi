package com.stock.trade.api.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.junit.Test;

import com.stock.trade.api.domain.Stock;
import com.stock.trade.api.rest.domain.StockReport;

public class StockReportUtilsTest {

	@Test
	public void getStockReport() {
		List<Stock> list = new ArrayList<>();
		assertNull(StockReportUtils.getStockReport(null));
		assertNull(StockReportUtils.getStockReport(list));
		Stock stock = new Stock();
		list.add(stock);
		assertNotNull(StockReportUtils.getStockReport(list));
		
		//High stock
		stock.setPrice(new BigDecimal(10));
		stock.setSymbol("AC");
		StockReport report = StockReportUtils.getStockReport(list);
		assertTrue(Objects.equals(report.getStock(), "AC"));
		Stock stock2 = new Stock();
		stock2.setSymbol("AC");
		stock2.setPrice(BigDecimal.valueOf(15));
		list.add(stock2);
		report = StockReportUtils.getStockReport(list);
		assertTrue(Objects.equals(report.getStock(), "AC"));
		assertTrue(Objects.equals(report.getMax_rise(), BigDecimal.valueOf(5)));
		Stock stock3 = new Stock();
		stock3.setSymbol("AC");
		stock3.setPrice(BigDecimal.valueOf(19));
		list.add(stock3);
		report = StockReportUtils.getStockReport(list);
		assertTrue(Objects.equals(report.getStock(), "AC"));
		assertTrue(Objects.equals(report.getMax_rise(), BigDecimal.valueOf(5)));
		
		//Max low stocks
		list.clear();
		stock.setSymbol("AC");
		stock.setPrice(BigDecimal.valueOf(10));
		list.add(stock);
		stock2.setSymbol("AC");
		stock2.setPrice(BigDecimal.valueOf(5));
		list.add(stock2);
		stock3.setSymbol("AC");
		stock3.setPrice(BigDecimal.valueOf(3));
		list.add(stock3);
		report = StockReportUtils.getStockReport(list);
		assertTrue(Objects.equals(report.getStock(), "AC"));
		assertTrue(Objects.equals(report.getMax_fall(), BigDecimal.valueOf(5)));
		
		//Random
		list.clear();
		stock.setSymbol("AC");
		stock.setPrice(BigDecimal.valueOf(10));
		list.add(stock);
		stock2.setSymbol("AC");
		stock2.setPrice(BigDecimal.valueOf(11));
		list.add(stock2);
		stock3.setSymbol("AC");
		stock3.setPrice(BigDecimal.valueOf(7));
		list.add(stock3);
		Stock stock8 = new Stock();
		stock8.setSymbol("AC");
		stock8.setPrice(BigDecimal.valueOf(6));
		list.add(stock8);
		Stock stock4 = new Stock();
		stock4.setSymbol("AC");
		stock4.setPrice(BigDecimal.valueOf(14));
		list.add(stock4);
		Stock stock5 = new Stock();
		stock5.setSymbol("AC");
		stock5.setPrice(BigDecimal.valueOf(5));
		list.add(stock5);
		Stock stock6 = new Stock();
		stock6.setSymbol("AC");
		stock6.setPrice(BigDecimal.valueOf(20));
		list.add(stock6);
		Stock stock7 = new Stock();
		stock7.setSymbol("AC");
		stock7.setPrice(BigDecimal.valueOf(1));
		list.add(stock7);
		report = StockReportUtils.getStockReport(list);
		assertTrue(Objects.equals(report.getStock(), "AC"));
		assertTrue(Objects.equals(report.getMax_fall(), BigDecimal.valueOf(19)));
		assertTrue(Objects.equals(report.getMax_rise(), BigDecimal.valueOf(15)));
		assertTrue(Objects.equals(report.getFluctuations(), 5));
		
		
	}

}
