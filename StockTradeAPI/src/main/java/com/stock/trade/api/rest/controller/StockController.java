package com.stock.trade.api.rest.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stock.trade.api.domain.Stock;
import com.stock.trade.api.domain.TradeSymbol;
import com.stock.trade.api.domain.User;
import com.stock.trade.api.reporsitory.StockRepository;
import com.stock.trade.api.reporsitory.TradeSymbolRepository;
import com.stock.trade.api.reporsitory.UserRepository;
import com.stock.trade.api.rest.domain.StockRange;
import com.stock.trade.api.rest.domain.StockReport;
import com.stock.trade.api.utils.StockReportUtils;

@RestController
public class StockController {
	private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);

	@Autowired
	private StockRepository stockRepo;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TradeSymbolRepository tradeSymbolRepository;

	@GetMapping("trades")
	public ResponseEntity<List<Stock>> findAll() {
		LOGGER.debug("Retriving all the Trade infomation");
		List<Stock> list = stockRepo.findAll();
		LOGGER.info("Total Stock records" + list.size());
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@GetMapping("trades/users/{userID}")
	public ResponseEntity<List<Stock>> findByUserID(@PathVariable("userID") Long userID) {
		LOGGER.debug("Retriving all the Trade infomation assigned to user:" + userID);

		Optional<User> optionalUser = userRepository.findById(userID);
		User user = optionalUser.isPresent() ? optionalUser.get() : null;

		if (user == null) {
			LOGGER.error("User is not present with specified ID:" + userID);
			throw new RuntimeException("User is not present with specified ID:" + userID);
		}

		List<Stock> list = stockRepo.findByUser(user);
		LOGGER.info("Total Stock records" + list.size());

		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@PostMapping("trades")
	public ResponseEntity<Stock> addStock(@RequestBody @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Stock stock) {

		User user = userRepository.findByNameAndId(stock.getUser().getName(), stock.getUser().getId());

		if (user == null) {
			LOGGER.error("User is not present with specified ID:" + stock.getUser().getId() + " user Name:"
					+ stock.getUser().getName());
			throw new RuntimeException("User is not present with specified ID:" + stock.getUser().getId()
					+ " user Name:" + stock.getUser().getName());
		}

		if (!tradeSymbolRepository.existsBySymbol(stock.getSymbol())) {
			LOGGER.error("Invalid Trade symbol" + stock.getSymbol());
			throw new RuntimeException("Invalid Trade symbol:" + stock.getSymbol());
		}
		stock.setUser(user);
		stock.setIsDeleted(false);
		stock = stockRepo.save(stock);

		return new ResponseEntity<>(stock, HttpStatus.OK);
	}

	@GetMapping("stocks/{stockSymbol}/price")
	public ResponseEntity<StockRange> findStockSymbolTradesWithDateRange(
			@PathVariable("stockSymbol") String stockSymbol,
			@RequestParam(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
			@RequestParam(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end) {
		LOGGER.debug("Retriving all the Trade infomation assigned to stockSymbol:" + stockSymbol + "start Date:" + start
				+ "end Date :" + end);
		StockRange range = new StockRange();
		List<Stock> list = stockRepo.findBySymbolAndTimestampBetween(stockSymbol, start.atStartOfDay(),
				end.atTime(LocalTime.MAX));

		if (list.isEmpty()) {
			throw new RuntimeException("There are no trades in the given date range.");
		}

		range.setSymbol(stockSymbol);
		range.setHighest(list.stream().max(Comparator.comparing(Stock::getPrice)).get().getPrice());
		range.setLowest(list.stream().min(Comparator.comparing(Stock::getPrice)).get().getPrice());
		return new ResponseEntity<>(range, HttpStatus.OK);
	}

	@GetMapping("stocks/stats")
	public ResponseEntity<List<StockReport>> findTradesWithDateRange(
			@RequestParam(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
			@RequestParam(required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end) {
		LOGGER.debug("Retriving all the Trade infomation between start Date:" + start + "end Date :" + end);
		List<StockReport> stockReport = new ArrayList<>();

		List<Stock> stocks = stockRepo.findByTimestampBetween(start.atStartOfDay(), end.atTime(LocalTime.MAX));

		List<TradeSymbol> symbols = tradeSymbolRepository.findAll();
		List<String> symbolList = symbols.stream().map(s -> s.getSymbol()).collect(Collectors.toList());

		Map<String, List<Stock>> mappedData = new HashMap<>();
		
		stocks.forEach(s -> {
			List<Stock> groupedStocks = null;
			if (mappedData.containsKey(s.getSymbol())) {
				groupedStocks = mappedData.get(s.getSymbol());
			} else {
				groupedStocks = new ArrayList<>();
			}
			groupedStocks.add(s);
			mappedData.put(s.getSymbol(), groupedStocks);
		});

		mappedData.forEach((key, value) -> {
			symbolList.remove(key);
			stockReport.add(StockReportUtils.getStockReport(value));
		});

		if (!symbolList.isEmpty()) {
			symbolList.forEach(s -> {
				StockReport report = new StockReport();
				report.setStock(s);
				report.setMessage("There are no trades in the given range");
				stockReport.add(report);
			});
		}

		return new ResponseEntity<>(stockReport, HttpStatus.OK);
	}

	@DeleteMapping("erase")
	public String delete() {
		LOGGER.debug("Deleting all the trades from DB");
		List<Stock> list = stockRepo.findAll();
		for (Stock stock : list) {
			stock.setDeletedOn(LocalDate.now());
			stock.setIsDeleted(true);
			stockRepo.save(stock);
		}
		return "Records deleted successfully";
	}

}
