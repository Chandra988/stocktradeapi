package com.stock.trade.api.utils;

import java.math.BigDecimal;
import java.util.List;

import com.stock.trade.api.domain.Stock;
import com.stock.trade.api.rest.domain.StockReport;

public class StockReportUtils {
	
	private StockReportUtils() {
		
	}

	public static StockReport getStockReport(List<Stock> list) {
		StockReport report = null;
		if (list != null && !list.isEmpty()) {
			report = new StockReport();
			BigDecimal maxFall = BigDecimal.ZERO;
			BigDecimal maxRise = BigDecimal.ZERO;
			Integer fluctuations = 0;
			BigDecimal previousPrice = BigDecimal.ZERO;
			Boolean down = false;
			Boolean up = false;
			for (Stock stock : list) {
				report.setStock(stock.getSymbol());
				BigDecimal currentPrice = stock.getPrice();
				if (!previousPrice.equals(BigDecimal.ZERO)) {
					if (previousPrice.compareTo(currentPrice) > 0) {
						BigDecimal fallAmount = previousPrice.subtract(currentPrice);
						if (maxFall.compareTo(fallAmount) < 0) {
							maxFall = fallAmount;
						}
						down = true;
						if (down && up) {
							fluctuations += 1;
							up = false;
						}

					} else {
						BigDecimal raiseAmount = currentPrice.subtract(previousPrice);
						if (maxRise.compareTo(raiseAmount) < 0) {
							maxRise = raiseAmount;
						}
						up = true;
						if (down && up) {
							fluctuations += 1;
							down = false;
						}

					}
				}
				previousPrice = currentPrice;

			}
			report.setFluctuations(fluctuations);
			report.setMax_fall(maxFall);
			report.setMax_rise(maxRise);
		}
		return report;
	}
}
