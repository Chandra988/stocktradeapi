package com.stock.trade.api.rest.controller;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.stock.trade.api.annotation.ActivieProfileTest;
import com.stock.trade.api.constants.URLConstants;
import com.stock.trade.api.domain.Stock;
import com.stock.trade.api.domain.User;
import com.stock.trade.api.rest.domain.StockRange;
import com.stock.trade.api.rest.domain.StockReport;
import com.stock.trade.api.utils.RestTemplateUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActivieProfileTest
public class StockControllerTest {
	
	RestTemplate template = RestTemplateUtil.getRestTemplate();
	
	@Autowired
	private StockController controller;

	@Test
	public void allStocks() {
		String url = URLConstants.URL + URLConstants.ALL_TRADES;
        ParameterizedTypeReference<List<Stock>> newTag = new ParameterizedTypeReference<List<Stock>>(){};
		ResponseEntity<List<Stock>> allStocks = RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.GET, null, newTag);
		assertEquals(true, Objects.equals(allStocks.getStatusCode(), HttpStatus.OK));
		List<Stock> stocks = allStocks.getBody();
		assertEquals(true, Objects.equals(stocks.size(), 11));
	}
	
	@Test
	public void findByUserID() {
		String url = URLConstants.URL + URLConstants.FIND_TRADES_WITH_USER_ID;
        url = MessageFormat.format(url, 1);
        ParameterizedTypeReference<List<Stock>> newTag = new ParameterizedTypeReference<List<Stock>>(){};
		ResponseEntity<List<Stock>> allStocks = RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.GET, null, newTag);
		assertEquals(true, Objects.equals(allStocks.getStatusCode(), HttpStatus.OK));
		List<Stock> stocks = allStocks.getBody();
		assertEquals(true, Objects.equals(stocks.size(), 4));
	}
	
	@Test(expected = Exception.class)
	public void findByUserIDException() {
		String url = URLConstants.URL + URLConstants.FIND_TRADES_WITH_USER_ID;
        url = MessageFormat.format(url, 100);
        ParameterizedTypeReference<List<Stock>> newTag = new ParameterizedTypeReference<List<Stock>>(){};
		RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.GET, null, newTag);
	}
	
	@Test
	public void addStock() {
		String url = URLConstants.URL + URLConstants.ALL_TRADES;
        url = MessageFormat.format(url, 1);
        Stock stock = new Stock();
        stock.setType("Buy");
		stock.setPrice(BigDecimal.valueOf(146.09));
		stock.setSymbol("ACC");
		stock.setShares(25);
		stock.setTimestamp(LocalDateTime.of(2016, 06, 25, 13, 40, 13));
		User user = new User();
		user.setName("David");
		user.setId((long)1);
		stock.setUser(user);
		ResponseEntity<Stock> allStocks =RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.POST, RestTemplateUtil.getCreateHeaders(stock), Stock.class);
		assertEquals(true, Objects.equals(allStocks.getStatusCode(), HttpStatus.OK));
		Stock stocks = allStocks.getBody();
		assertEquals(true, Objects.equals(stocks.getId(), (long)11));
	}
	
	@Test(expected = Exception.class)
	public void addStockNoUser() {
		String url = URLConstants.URL + URLConstants.ALL_TRADES;
        url = MessageFormat.format(url, 1);
        Stock stock = new Stock();
        stock.setType("Buy");
		stock.setPrice(BigDecimal.valueOf(146.09));
		stock.setSymbol("ACC");
		stock.setShares(25);
		stock.setTimestamp(LocalDateTime.of(2016, 06, 25, 13, 40, 13));
		User user = new User();
		user.setName("Chandra");
		user.setId((long)1);
		stock.setUser(user);
		RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.POST, RestTemplateUtil.getCreateHeaders(stock), Stock.class);
	}
	
	@Test(expected = Exception.class)
	public void addStockNoSymbol() {
		String url = URLConstants.URL + URLConstants.ALL_TRADES;
        url = MessageFormat.format(url, 1);
        Stock stock = new Stock();
        stock.setType("Buy");
		stock.setPrice(BigDecimal.valueOf(146.09));
		stock.setSymbol("ACCC");
		stock.setShares(25);
		stock.setTimestamp(LocalDateTime.of(2016, 06, 25, 13, 40, 13));
		User user = new User();
		user.setName("David");
		user.setId((long)1);
		stock.setUser(user);
		RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.POST, RestTemplateUtil.getCreateHeaders(stock), Stock.class);
	}
	
	@Test
	public void findStockSymbolTradesWithDateRange() {
		String url = URLConstants.URL + "/stocks/AC/price?start=2014-06-25&end=2014-06-26";
		ParameterizedTypeReference<StockRange> newTag = new ParameterizedTypeReference<StockRange>(){};
		ResponseEntity<StockRange> stockRangeResponse = RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.GET, null, newTag);
		assertEquals(true, Objects.equals(stockRangeResponse.getStatusCode(), HttpStatus.OK));
		StockRange  range = stockRangeResponse.getBody();
		assertEquals(true, Objects.equals(range.getSymbol(), "AC"));
		assertEquals(true, Objects.equals(range.getHighest(), BigDecimal.valueOf(162.37)));
		assertEquals(true, Objects.equals(range.getLowest(), BigDecimal.valueOf(137.39)));
	}
	
	@Test(expected = Exception.class)
	public void findStockSymbolTradesWithDateRangeException() {
		String url = URLConstants.URL + "/stocks/AC/price?start=2013-06-25&end=2013-06-26";
		ParameterizedTypeReference<StockRange> newTag = new ParameterizedTypeReference<StockRange>(){};
		RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.GET, RestTemplateUtil.getCreateHeaders(null), newTag);
	}
	
	@Test
	public void findTradesWithDateRange() {
		String url = URLConstants.URL + "/stocks/stats?start=2014-06-25&end=2014-06-26";
		ParameterizedTypeReference<List<StockReport>> newTag = new ParameterizedTypeReference<List<StockReport>>(){};
		ResponseEntity<List<StockReport>> stockRangeResponse = RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.GET, null, newTag);
		assertEquals(true, Objects.equals(stockRangeResponse.getStatusCode(), HttpStatus.OK));
		List<StockReport>  reports = stockRangeResponse.getBody();
		assertEquals(true, Objects.equals(reports.size(), 3));
		
		url = URLConstants.URL + "/stocks/stats?start=2014-06-25&end=2014-06-27";
		stockRangeResponse = RestTemplateUtil.getRestTemplate().exchange(url, HttpMethod.GET, null, newTag);
		assertEquals(true, Objects.equals(stockRangeResponse.getStatusCode(), HttpStatus.OK));
		reports = stockRangeResponse.getBody();
		assertEquals(true, Objects.equals(reports.size(), 3));
	}
	
	@Test
	public void lastErase() {
		String message = controller.delete();
		assertEquals(true, Objects.equals(message, "Records deleted successfully"));
	}
}
